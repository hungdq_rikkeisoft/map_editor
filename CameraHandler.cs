﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHandler : MonoBehaviour
{

    private bool _lHeld = false;
    private bool _rHeld = false;
    private bool _mHeld = false;

    private float _curZoomPos, _zoomTo = 0f;
    private float _zoomFrom = 40f;

    private Vector3 _lastMousePos = Vector3.zero;

    private Vector3 _startMousePos;
    private Vector3 _startCamPos;
    private Quaternion _startCamRotation;

    public Camera cam;

    [SerializeField]
    private float _rotateSpeed = 50;

    private void Awake()
    {
        cam = GetComponent<Camera>();
        _startCamPos = cam.transform.position;
        _startCamRotation = cam.transform.rotation;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _lHeld = true;
            _startMousePos = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            _lHeld = false;
            _startMousePos = Vector3.zero;
        }

        if (Input.GetMouseButtonDown(1))
        {
            _rHeld = true;
            _startMousePos = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(1))
        {
            _rHeld = false;
            _startMousePos = Vector3.zero;
        }

        if (Input.GetMouseButtonDown(2))
        {
            _mHeld = true;

            _startMousePos = Input.mousePosition;
            _startCamPos = cam.transform.position;
        }
        else if (Input.GetMouseButtonUp(2))
        {
            _mHeld = false;
            _startMousePos = Vector3.zero;
            _startCamPos = cam.transform.position;
        }

        if (_lHeld)
        {
            //Handle left mouse funct
        }

        if (_rHeld)
        {
            rotateCamera();
        }

        if (_mHeld)
        {
            moveCamera();
        }

        zoomCamera();
    }

    private void zoomCamera()
    {
        float _deltaScroll = Input.mouseScrollDelta.y;
        // Debug.Log(_deltaScroll);
        if(_deltaScroll >= 1) { 
            _zoomTo -= 5f;
        }
        else if(_deltaScroll <= -1) {
            _zoomTo += 5f;
        }

        _curZoomPos = _zoomFrom + _zoomTo;

        _curZoomPos = Mathf.Clamp(_curZoomPos, 5f, 50f);

        _zoomTo = Mathf.Clamp(_zoomTo, -45f, 45f);

        cam.fieldOfView = _curZoomPos;

    }

    private void rotateCamera()
    {
        cam.transform.Rotate(cam.transform.up, -Mathf.Deg2Rad * (_rotateSpeed * Input.GetAxis("Mouse X")), Space.World);
        cam.transform.Rotate(cam.transform.right, Mathf.Deg2Rad * (_rotateSpeed * Input.GetAxis("Mouse Y")), Space.World);
        float z = cam.transform.eulerAngles.z;
        cam.transform.Rotate(0, 0, -z);
    }

    private void moveCamera()
    {
        Vector3 _dir = Input.mousePosition - _startMousePos;
        _dir /= 50;
        Vector3 _offset = _dir.x * cam.transform.right + _dir.y * cam.transform.up;
        cam.transform.position = _startCamPos - _offset;
    }

}
