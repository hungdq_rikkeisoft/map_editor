﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InspectorButton : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject panel;
    public bool isActive = true;
    public void select()
    {
        isActive = !isActive;
        panel.SetActive(isActive);
    }
}
