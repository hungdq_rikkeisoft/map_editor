﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDialogButton : MonoBehaviour
{
    public GameObject panel;

    public bool isActive = true;
    public void select()
    {
        panel.SetActive(isActive);
    }
}
