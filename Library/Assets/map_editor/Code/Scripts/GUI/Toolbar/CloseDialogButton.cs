﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseDialogButton : MonoBehaviour
{
    public GameObject panel;

    public bool isActive = false;
    public void select()
    {
        panel.SetActive(isActive);
    }
}
