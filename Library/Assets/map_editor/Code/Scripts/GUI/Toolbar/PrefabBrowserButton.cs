﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabBrowserButton : MonoBehaviour
{
    public GameObject panel;
    public bool isActive = true;
    public void select()
    {
        isActive = !isActive;
        panel.SetActive(isActive);
    }
}
