﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{

    private bool _lHeld = false;
    private bool _rHeld = false;
    private bool _mHeld = false;

    private Vector3 _startMousePos;
    private Vector3 _startCamPos;
    private Quaternion _startCamRotation;

    public Camera cam;

    [SerializeField]
    private float _rotateSpeed = 30;
    
    private void Awake()
    {
        cam = GetComponent<Camera>();
        _startCamPos = cam.transform.position;
        _startCamRotation = cam.transform.rotation; 
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _lHeld = true;
            _startMousePos = Input.mousePosition;
        } else if (Input.GetMouseButtonUp(0))
        {
            _lHeld = false;
            _startMousePos = Vector3.zero;
        }

        if (Input.GetMouseButtonDown(1))
        {
            _rHeld = true;
            _startMousePos = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(1))
        {
            _rHeld = false;
            _startMousePos = Vector3.zero;
        }

        if (Input.GetMouseButtonDown(2))
        {
            _mHeld = true;
            _startMousePos = Input.mousePosition;
            _startCamPos = cam.transform.position;
        }
        else if (Input.GetMouseButtonUp(2))
        {
            _mHeld = false;
            _startMousePos = Vector3.zero;
            _startCamPos = cam.transform.position;
        }

        if (_lHeld)
        {
            //Handle left mouse funct
        }

        if(_rHeld)
        {
            cam.transform.Rotate(cam.transform.up, -Mathf.Deg2Rad * (_rotateSpeed * Input.GetAxis("Mouse X")), Space.World);
            cam.transform.Rotate(cam.transform.right, Mathf.Deg2Rad * (_rotateSpeed * Input.GetAxis("Mouse Y")), Space.World);
            float z = cam.transform.eulerAngles.z;
            cam.transform.Rotate(0, 0, -z);
            //cam.transform.Rotate(0, Input.GetAxis("Mouse X") * _rotateSpeed, 0);
            //cam.transform.Rotate(-Input.GetAxis("Mouse Y") * _rotateSpeed, 0, 0);
        }

        if (_mHeld)
        {
            Vector3 _dir = Input.mousePosition - _startMousePos;
            cam.transform.position = _startCamPos - _dir;
        }

    }
}
